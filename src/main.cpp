#include <iostream>
#include "game.h"

using namespace std;

template <unsigned X, unsigned Y, typename T>
ostream &operator<<(ostream &o, Field<X, Y, T> f)
{
    auto a = f.get_array();
    for (auto x = 0; x < a.size(); ++x)
    {
        o << (a[x] ? 'X' : '-');
        if ((x % X) == X - 1)
            o << '\n';
    }
    return o;
}


int main(int argc, char *argv[])
{

   array<bool, 10*10> ar =
    {
        false, false, false, false, false, false, false, false, false, false, 
        false, false, false, false, false, false, false, false, false, false, 
        false, false, false, false, false, false, false, false, false, false, 
        false, false, false, false, true, false, false, false, false, false, 
        false, false, true, false, true, false, false, false, false, false, 
        false, false, true, false, true, false, false, false, false, false, 
        false, false, false, false, false, true, false, false, false, false, 
        false, false, false, false, false, false, true, true, false, false, 
        false, false, false, false, false, false, false, false, false, false, 
        false, false, false, false, false, false, false, false, false, false, 
    };

    Game<10, 10> game( Field<10, 10, bool>{ar} );

    cout << game.get_field();

    while (true)
    {
        game.step_over();
        cout << game.get_field() << "\n\n\n";
    }
    return 0;
}