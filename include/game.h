#ifndef _GAME_H_
#define _GAME_H_

#include "field.h"

enum State : bool
{
    Life = true ,
    Dead = false
};

template <unsigned X, unsigned Y>
class Game
{
  public:
    using Field_type = bool;

    Game() {}
    Game(Field<X,Y,Field_type> f) :field(f) {}

    Field<X,Y, Field_type>& get_field() { return this->field; }
    void step_over();
protected:
    Field<X, Y, Field_type> field;
};

template <unsigned X, unsigned Y>
void Game<X,Y>::step_over()
{
    Field<X,Y,Field_type> old_field (field);

    for(int x = 0; x < X; ++x)
        for(int y = 0; y < Y; ++y)
        {

            auto in_life = old_field.how_much_neighbours_is(Coordinate{x, y}, State::Life);
            switch(in_life)
            {
                case 0: 
                case 1:
                    field.get_value( {x, y}) = State::Dead;
                    break;
                case 2:
                    break;
                case 3: 
                    if(old_field.get_value( {x, y} ) == State::Dead)
                        field.get_value( {x, y}) = State::Life;
                    
                    break;
                case 4: 
                case 5: 
                case 6: 
                case 7: 
                case 8: 
                    field.get_value( {x, y}) = State::Dead;                    
                    break;
            }
        }
    
    // TODO
}

#endif // !_GAME_H_