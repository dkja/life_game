#ifndef _COORDINATE_H_
#define _COORDINATE_H_

struct Coordinate
{
public:
    signed x, y;
    Coordinate round(unsigned x, unsigned y)
    {
        if(this->x < 0)
            this->x = x - (-this->x % x);
        else
            this->x %= x;

        if(this->y < 0)
            this->y = y - (-this->y % y);
        else
            this->y %= y;

        return *this;
    }

};

#endif // !_COORDINATE_H_