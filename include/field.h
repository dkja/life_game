#ifndef _FIELD_H_
#define _FIELD_H_

#include <array>
#include "coordinate.h"

using std::array;

template<unsigned X, unsigned Y, typename T>
class Field
{
public:
    Field(T def) 
    {
        for(auto &x : this->data)
            x = def;

    }
    Field() :Field( T{} ) {}
    Field( const array<T, X*Y>& a ) : data(a) {}
    Field( const Field<X,Y,T>& o )  : data(o.data) {}

    const array<T, X*Y>& get_array() const;
    unsigned how_much_neighbours_is(Coordinate cord,T val);
    T&          get_value(Coordinate cord);

private:
    unsigned get_addr(Coordinate cord);
protected:
    array<T, X*Y> data;
};

template<unsigned X, unsigned Y, typename T>
const array<T, X*Y>& Field<X, Y, T>::get_array() const
{
    return this->data;
}

template<unsigned X, unsigned Y, typename T>
T&  Field<X,Y,T>::get_value(Coordinate cord)
{
    return data[ get_addr(cord) ];
}


template<unsigned X, unsigned Y, typename T>
unsigned Field<X,Y,T>::how_much_neighbours_is(Coordinate cord, T val)
{
    unsigned res{};
    auto addr= 0;
  
  auto loop = [&]
    {
        cord.round(X,Y);
        addr = get_addr(cord);
        if(data[addr] == val)
            ++res;
    };
     
    ++(cord.x);  // >>
    loop();     //
    ++(cord.y);  // vv
    loop();     //
    --(cord.x);  // <<
    loop();     //
    --(cord.x);  // <<
    loop();     //
    --(cord.y);  // ^^
    loop();     //
    --(cord.y);  // ^^
    loop();     //
    ++(cord.x);  // >>
    loop();     //
    ++(cord.x);  // >>
    loop();     //

    return res;
}

template<unsigned X, unsigned Y, typename T>
unsigned Field<X,Y,T>::get_addr(Coordinate cord)
{
    return cord.y*X + cord.x;
}






#endif // !_FIELD_H_